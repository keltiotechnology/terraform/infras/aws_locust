locals {
  tag = "Locust"
  cmd = <<EOT
  #!/bin/bash
  apt update
  apt install -y python3-pip
  pip3 install locust
EOT
}

provider "aws" {
  region  = var.region
  profile = var.aws_profile
}

/* VPC */
module "vpc" {
  source                 = "cloudposse/vpc/aws"
  version                = "0.26.1"
  cidr_block             = var.vpc_cidr_block
  namespace              = var.namespace
  name                   = var.vpc_name
  security_group_enabled = false

  tags = {
    Name = local.tag
  }
}

module "subnets" {
  source              = "cloudposse/dynamic-subnets/aws"
  version             = "0.39.3"
  availability_zones  = var.availability_zones
  vpc_id              = module.vpc.vpc_id
  igw_id              = module.vpc.igw_id
  cidr_block          = module.vpc.vpc_cidr_block
  nat_gateway_enabled = true

  depends_on = [
    module.vpc
  ]

  tags = {
    Name = local.tag
  }
}

resource "aws_security_group" "locust" {
  name        = local.tag
  description = "Security group for locust instance"
  vpc_id      = module.vpc.vpc_id

  # Allow incoming HTTP traffic from anywhere
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # Allow incoming HTTPS traffic from anywhere
  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # Allow incoming SSH traffic from anywhere (WARNING : not secure, so do not use this security group for other instances)
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # Allow any outcoming traffic
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = local.tag
  }
}

resource "aws_instance" "locust" {
  ami                         = "ami-0f7cd40eac2214b37"
  instance_type               = var.instance_type
  availability_zone           = var.availability_zones[0]
  key_name                    = aws_key_pair.locust.key_name
  associate_public_ip_address = true
  subnet_id                   = module.subnets.public_subnet_ids[0]
  user_data                   = base64encode(local.cmd)
  vpc_security_group_ids = [
    aws_security_group.locust.id
  ]

  root_block_device {
    volume_size = 30
  }

  tags = {
    Name = local.tag
  }
}

resource "aws_key_pair" "locust" {
  key_name   = var.key_name
  public_key = var.public_key
}

resource "aws_eip" "web_instance_floating_ip" {
  vpc      = true
  instance = aws_instance.locust.id
}
