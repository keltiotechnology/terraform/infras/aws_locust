# Sample input definition
region             = "eu-west-3"
availability_zones = ["eu-west-3a"]
instance_type      = "t2.small"
vpc_cidr_block     = "172.16.0.0/16"
namespace          = "locust"
vpc_name           = "vpc"
key_name           = "locust_ops"
public_key         = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDw94BxfEKaWSnqweKRzp6c8OSeT9wpxcWP+tCKctnN1GSCBfIVXnFBLfzO0ntI8FNtbqoRdqvOevlvI7LZpG7N+OoprT6KhTpSR8tfhuUfaiRdLfjeR8mnCVwmWxpT/xXc0g+qp7th6ZcR7ManjxsSxPLMrAdRIO0HNLHAS+ZP97OkkfSpPKrCGYss8wTedPk/IXnOMAXUw0Hj29ALx+9U0WmmHAZBhQDBOvG3FHCgXLyB4UHYutcVLxrHIuorsWQERvuzxITx1kBzmrn+ufBUA/8CxW5hWCT/5bQ2H1v/zoPRRcFue69CwSsIifKeltWiGjtJDv7E6lxPZBPBLtnKs4WL0pZ1xg1vpoHP0ql9d5+Wmxvn/B2N4cesa/aHeP35muGunupkQYHPZUGUE+vxYPffTVywwbDl6DE50oDzRRAbB0kGEcJTKA6522RzTV3nIZxaJelQ2Tvic54zLxwvWFlIKbVYuTgnwPeKO8GfuJOeIGmiqf2gUoko1F4KRgy8XxdlLPEsedYXMrb8fu8x+ZEs+r8iGQGPH3mattkMOcDh7qlh1PX/zTc4D4bvQh6tzjp8MaCxxeZelVBT4/Ie6cl2RDA5U731EqydwGUX9w098HNgaEWR9+ODumM0BefgfHQk+PM6TGf7tjdx4XmD+SOt9rHga1y3d14CZtTszQ== kevin@keltio.fr"
