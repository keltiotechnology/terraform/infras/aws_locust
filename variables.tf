variable "aws_profile" {
  type        = string
  description = "AWS credential profile"
  default     = "default"
}

variable "region" {
  type        = string
  description = "AWS region"
}

variable "namespace" {
  type        = string
  description = "Namespace, which could be your organization name or abbreviation, e.g. 'eg' or 'cp'"
}

variable "vpc_name" {
  type        = string
  description = "Solution name, e.g. 'app' or 'jenkins'"
}

variable "vpc_cidr_block" {
  type        = string
  description = "VPC CIDR block"
}

variable "availability_zones" {
  type        = list(string)
  description = "List of availability zones"
}

variable "instance_type" {
  type        = string
  description = "Flavor of the locust instance"
}

variable "key_name" {
  type        = string
  description = "Name to give to the ssh key associated to the locust instance in AWS"
  default     = "locust"
}

variable "public_key" {
  type        = string
  description = "The public key of the ssh key associated to the locust instance"
}
